#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='wenworld_utils',
      version='1.1',
      packages=find_packages(),
      exclude_package_data={'wenworld_utils': ['*.pyc']},
      include_package_data=True)
