import os
import ftplib
import itertools
import logging
import threading
import urlparse

log = logging.getLogger(__name__)


def create_ftp_connection(server_url):
    """
    Login to FTP server using a connection string in the form::

        ftp://user:password@server.example.com

    """
    parsed = urlparse.urlparse(server_url)
    ftp = ftplib.FTP(parsed.hostname)
    ftp.login(parsed.username, parsed.password)
    ftp.server_url = server_url
    log.debug("Connected to %s as %s", parsed.hostname, parsed.username)
    return ftp


def _batch(iterable, batches=10):
    """Split iterable into `batches` groups"""
    args = [iter(iterable)] * batches
    return ([i for i in t if i is not None] for t
            in itertools.izip_longest(*args))


def get_file_list(ftp_conn, source='', destination='.'):
    """Recursively discover files from FTP to destination"""

    source = source or ftp_conn.pwd()
    ftp_conn.cwd(source)
    log.debug('Crawling directory %s', source)

    # trailing and preceeding slashes cause issues, hence the strips
    output_dir = os.path.join(destination, source.strip('/')).rstrip('/')

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    files_to_download = []
    for remote_file in ftp_conn.nlst():
        remote_filepath = os.path.join(source, remote_file)
        try:
            # If it is a directory, recurse through it
            ftp_conn.cwd(remote_filepath + '/')
            for l, r in get_file_list(ftp_conn, remote_filepath, destination):
                files_to_download.append(
                    (l, os.path.join(remote_filepath, r)))
        except ftplib.error_perm:
            # If it is not a directory, download it
            local_filepath = os.path.join(destination,
                                          remote_filepath.strip('/'))
            files_to_download.append((local_filepath, remote_filepath))

    return files_to_download


def download_file(ftp_conn, local_filepath, remote_filepath):
    """Download a single file from the FTP connection"""
    with open(local_filepath, "wb") as new_file:
        ftp_conn.retrbinary("RETR " + remote_filepath, new_file.write)
        log.info('Wrote %s', local_filepath)


def _download_file_batch(server_url, local_remote_pairs):
    """Download a batch of remote files in a new FTP connection"""
    ftp_conn = create_ftp_connection(server_url)
    for local_filepath, remote_filepath in local_remote_pairs:
        download_file(ftp_conn, local_filepath, remote_filepath)
    ftp_conn.quit()


def download_files(ftp_conn, local_remote_pairs, threads=0):
    """Download files, either threaded (faster) or synchronous"""
    if threads:
        thread_tracker = []
        for file_batch in _batch(local_remote_pairs, threads):
            thread = threading.Thread(target=_download_file_batch,
                                      args=(ftp_conn.server_url, file_batch))
            thread.start()
            thread_tracker.append(thread)
        for t in thread_tracker:
            t.join()
    else:
        for local_filepath, remote_filepath in local_remote_pairs:
            download_file(ftp_conn, local_filepath, remote_filepath)


def download_directory(ftp_conn, local_dir, remote_dir, threads=0):
    """
    Download directory, assuming it only contains files
    Faster than a sync which requires lots of operations
    """
    ftp_conn.cwd(remote_dir)
    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

    files_to_download = []
    for remote_file in ftp_conn.nlst():
        remote_path = os.path.join(remote_dir, remote_file)
        local_path = os.path.join(local_dir, remote_path)
        files_to_download.append([local_path, remote_path])
    download_files(ftp_conn, files_to_download, threads=threads)


def ftp_sync(server_url, local_dir, threads=0):
    """Download all files on the remote server to the local destination"""
    log.debug("Starting ftp sync to %s...", local_dir)
    ftp_conn = create_ftp_connection(server_url)
    files_to_download = get_file_list(ftp_conn, destination=local_dir)
    download_files(ftp_conn, files_to_download, threads=threads)
    ftp_conn.quit()
